import "./App.css";
import BaiTapPhoneShop from "./BaiTapPhoneShop/BaiTapPhoneShop";

function App() {
  return (
    <div className="App container">
      <BaiTapPhoneShop />
    </div>
  );
}

export default App;
