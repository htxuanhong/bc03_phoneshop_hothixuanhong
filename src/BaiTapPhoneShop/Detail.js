import React, { Component } from "react";

export default class Detail extends Component {
  render() {
    const { item } = this.props;

    if (!item) return undefined;

    const {
      tenSP,
      hinhAnh,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
    } = item;

    return (
      <div className="container ">
        <hr />
        <div class="row">
          <div class="col-sm-4 ">
            <h4 className="card-title">{tenSP}</h4>
            <img src={hinhAnh} alt="" className="card-img-top" />
          </div>
          <div className="col-sm-8 table text-left ">
            <table>
              <thead>
                <tr>
                  <th>Thông số kỹ thuật</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Màn hình</td>
                  <td>{manHinh}</td>
                </tr>
                <tr>
                  <td>Hệ điều hành</td>
                  <td>{heDieuHanh}</td>
                </tr>
                <tr>
                  <td>Camera trước</td>
                  <td>{cameraTruoc}</td>
                </tr>
                <tr>
                  <td>Camera sau</td>
                  <td>{cameraSau}</td>
                </tr>
                <tr>
                  <td>RAM</td>
                  <td>{ram}</td>
                </tr>
                <tr>
                  <td>ROM</td>
                  <td>{rom}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
