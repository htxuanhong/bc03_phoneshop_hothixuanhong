import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    const { handleItemClick } = this.props;

    let { tenSP, hinhAnh } = this.props.data;

    return (
      <div className="card col-4  ">
        <img src={hinhAnh} className="card-img-top h-75" alt="" />
        <div className="card-body h-25 text-left">
          <h5 className="card-title">{tenSP}</h5>

          <button
            className="btn btn-success mr-3"
            onClick={() => handleItemClick(this.props.data)}
          >
            Xem chi tiết
          </button>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.handleThemSanPham(this.props.data);
            }}
          >
            Thêm vào giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}
