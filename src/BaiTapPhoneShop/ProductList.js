import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    const { handleItemClick } = this.props;
    console.log("this.", this.props.productList);
    return (
      <div>
        <div className="row">
          {this.props.productList?.map((item, index) => {
            console.log(item);
            return (
              <ProductItem
                handleThemSanPham={this.props.handleThemSanPham}
                data={item}
                key={index}
                handleItemClick={handleItemClick}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
