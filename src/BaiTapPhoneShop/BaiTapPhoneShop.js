import React, { Component } from "react";
import Cart from "./Cart";
import dataPhone from "./dataPhone";
import Detail from "./Detail";
import ProductList from "./ProductList";

export default class BaiTapPhoneShop extends Component {
  state = {
    productList: dataPhone,
    selectedItem: null,
    gioHang: [],
  };
  handleThemSanPham = (sanPham) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP === sanPham.maSP;
    });
    if (index === -1) {
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioHang.push(newSanPham);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };
  _handleItemClick = (item) => {
    this.setState({
      selectedItem: item,
    });
  };
  _handleXoaSanPham = (maSanPham) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP === maSanPham;
    });
    if (index !== -1) {
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  handleUpDown = (maSanPham, giaTri) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP === maSanPham;
    });
    if (index !== -1) {
      cloneGioHang[index].soLuong += giaTri;
    }
    cloneGioHang[index].soLuong === 0 && cloneGioHang.splice(index, 1);

    this.setState({ gioHang: cloneGioHang });
  };
  render() {
    return (
      <div className="py-3">
        <h3 className="text-success">Cửa hàng PhoneShop xin chào!</h3>
        <div className="text-right">
          <button
            type="button"
            class="btn btn-outline-danger mb-2  "
            data-toggle="modal"
            data-target="#exampleModal"
          >
            Giỏ hàng ({this.state.gioHang.length})
          </button>

          {this.state.gioHang.length > 0 && (
            <Cart
              handleXoaSanPham={this._handleXoaSanPham}
              gioHang={this.state.gioHang}
              handleUpDown={this.handleUpDown}
            />
          )}
        </div>

        <ProductList
          handleItemClick={this._handleItemClick}
          handleThemSanPham={this.handleThemSanPham}
          productList={this.state.productList}
        />

        <Detail item={this.state.selectedItem} />
      </div>
    );
  }
}
