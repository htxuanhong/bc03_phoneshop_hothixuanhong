import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    const closeModalProps =
      this.props.gioHang.length === 1
        ? {
            "data-dismiss": "modal",
          }
        : {};

    return (
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Giỏ hàng
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <table
                className="table text-left"
                style={{ tableLayout: "fixed" }}
              >
                <thead>
                  <tr>
                    <th style={{ width: "125px" }}>Mã sản phẩm</th>
                    <th>Hình ảnh</th>
                    <th style={{ width: "130px" }}>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th style={{ width: "125px" }}>Thành tiền</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.gioHang.map((item) => {
                    return (
                      <tr>
                        <td>{item.maSP}</td>
                        <td>
                          <img src={item.hinhAnh} alt="" className="w-75 " />
                        </td>
                        <td>{item.tenSP}</td>
                        <td className="d-flex">
                          <button
                            className="btn btn-primary"
                            onClick={() => {
                              this.props.handleUpDown(item.maSP, 1);
                            }}
                          >
                            +
                          </button>
                          <span className="mx-1">{item?.soLuong}</span>
                          <button
                            className="btn btn-primary"
                            onClick={() => {
                              this.props.handleUpDown(item.maSP, -1);
                            }}
                          >
                            -
                          </button>
                        </td>
                        <td>{item.giaBan}</td>
                        <td>{item.giaBan * item?.soLuong}</td>
                        <td>
                          <button
                            className="btn btn-danger"
                            {...closeModalProps}
                            onClick={() => {
                              this.props.handleXoaSanPham(item.maSP);
                            }}
                          >
                            Xóa
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Đóng
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
